/*
There are four substring missions that were born all in one day and you shouldn’t be needed more than one day to solve them. All of those mission can be simply solved by brute force, but is it always the best way to go? (you might not have access to all of those missions yet, but they are going to be available with more opened islands on the map).

This mission is the first one of the series. Here you should find the length of the longest substring that consists of the same letter. For example, line "aaabbcaaaa" contains four substrings with the same letters "aaa", "bb","c" and "aaaa". The last substring is the longest one which makes it an answer.

Input: String.

Output: Int.
 */

let assert = require('assert');

assert.equal(longRepeat('sdsffffse'), 4, "First");
assert.equal(longRepeat('ddvvrwwwrggg'), 3, "Second");

function longRepeat(str) {
    str = str.split('');

    let counter = 0;
    let max = 0;
    let lastL = str[0];
    for(let l of str) {
        if(lastL === l) {
            if(++counter > max) {
                max = counter;
            }
        } else {
            counter = 1;
        }

        lastL = l;
    }

    return max;
}