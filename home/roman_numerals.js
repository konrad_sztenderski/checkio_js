/*
Roman numerals come from the ancient Roman numbering system. They are based on specific letters of the alphabet which are combined to signify the sum (or, in some cases, the difference) of their values. The first ten Roman numerals are:

I, II, III, IV, V, VI, VII, VIII, IX, and X.

The Roman numeral system is decimal based but not directly positional and does not include a zero. Roman numerals are based on combinations of these seven symbols:

Numeral	Value
I	1 (unus)
V	5 (quinque)
X	10 (decem)
L	50 (quinquaginta)
C	100 (centum)
D	500 (quingenti)
M	1,000 (mille)
More additional information about roman numerals can be found on the Wikipedia article.

For this task, you should return a roman numeral using the specified integer value ranging from 1 to 3999.

Input: A number as an integer.

Output: The Roman numeral as a string.
 */

let assert = require('assert');

assert.equal(romanNumerals(6), 'VI', "First");
assert.equal(romanNumerals(76), 'LXXVI', "Second");
assert.equal(romanNumerals(499), 'CDXCIX', "Third");
assert.equal(romanNumerals(3888), 'MMMDCCCLXXXVIII', "Forth");

function romanNumerals(num) {
    let s = '';

    let q = [
        {
            val: 3000,
            str: 'MMM'
        },
        {

            val: 2000,
            str: 'MM',
        },
        {

            val: 1000,
            str: 'M',
        },
        {

            val: 900,
            str: 'CM',
        },
        {

            val: 800,
            str: 'DCCC',
        },
        {

            val: 700,
            str: 'DCC',
        },
        {

            val: 600,
            str: 'DC',
        },
        {

            val: 500,
            str: 'D',
        },
        {

            val: 400,
            str: 'CD',
        },
        {

            val: 300,
            str: 'CCC',
        },
        {

            val: 200,
            str: 'CC',
        },
        {

            val: 100,
            str: 'C',
        },
        {

            val: 90,
            str: 'XC',
        },
        {

            val: 80,
            str: 'LXXX',
        },
        {

            val: 70,
            str: 'LXX',
        },
        {

            val: 60,
            str: 'LX',
        },
        {

            val: 50,
            str: 'L',
        },
        {

            val: 40,
            str: 'XL',
        },
        {

            val: 30,
            str: 'XXX',
        },
        {

            val: 20,
            str: 'XX',
        },
        {

            val: 10,
            str: 'X',
        },
        {

            val: 9,
            str: 'IX',
        },
        {

            val: 8,
            str: 'VIII',
        },
        {

            val: 7,
            str: 'VII',
        },
        {

            val: 6,
            str: 'VI',
        },
        {

            val: 5,
            str: 'V',
        },
        {

            val: 4,
            str: 'IV',
        },
        {

            val: 3,
            str: 'III',
        },
        {

            val: 2,
            str: 'II',
        },
        {

            val: 1,
            str: 'I',
        }
    ];

    for(let el of q) {
        if(num >= el.val) {
            s += el.str;
            num -= el.val;
        }
    }

    return s;
}