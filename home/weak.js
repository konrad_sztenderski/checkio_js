/*
The durability map is represented as a matrix with digits. Each number is the durability measurement for the cell. To find the weakest point we should find the weakest row and column. The weakest point is placed in the intersection of these rows and columns. Row (column) durability is a sum of cell durability in that row (column). You should find coordinates of the weakest point (row and column). The first row (column) is 0th row (column). If a section has several equal weak points, then choose the top left point.

Input: A durability map as a list of lists with integers.

Output: The coordinates of the weak point as a list (tuple) of integers.
 */

let assert = require('assert');

assert.deepEqual(weakPoint([[7, 2, 7, 2, 8],
    [2, 9, 4, 1, 7],
    [3, 8, 6, 2, 4],
    [2, 5, 2, 9, 1],
    [6, 6, 5, 4, 5]]
), [3, 3], "Example");
assert.deepEqual(weakPoint([[7, 2, 4, 2, 8],
    [2, 8, 1, 1, 7],
    [3, 8, 6, 2, 4],
    [2, 5, 2, 9, 1],
    [6, 6, 5, 4, 5]]
), [1, 2], "Two weak point");

assert.deepEqual(weakPoint([[1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]]
), [0, 0], "Top left");

function weakPoint(array) {
    let col;
    let row;
    let s_col;
    let s_row;

    for(let i = 0; i < array.length; i++) {
        let c_row = 0;
        let c_col = 0;
        for(let j = 0; j < array.length; j++) {
            c_row += array[i][j];
            c_col += array[j][i];
        }

        if(s_col === undefined) {
            s_col = c_col;
            col = i;
        } else if(c_col < s_col) {
            s_col = c_col;
            col = i;
        }

        if(s_row === undefined) {
            s_row = c_row;
            row = i;
        } else if(c_row < s_row) {
            s_row = c_row;
            row = i;
        }
    }

    return [row, col];
}