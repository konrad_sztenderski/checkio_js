/*
You are given a text, which contains different english letters and punctuation symbols. You should find the most frequent letter in the text. The letter returned must be in lower case.
While checking for the most wanted letter, casing does not matter, so for the purpose of your search, "A" == "a". Make sure you do not count punctuation symbols, digits and whitespaces, only letters.

If you have two or more letters with the same frequency, then return the letter which comes first in the latin alphabet. For example -- "one" contains "o", "n", "e" only once for each, thus we choose "e".

Input: A text for analysis as a string.

Output: The most frequent letter in lower case as a string.
 */

let assert = require('assert');

assert.equal(mostWanted("Hello World!"), "l", "1st example");
assert.equal(mostWanted("How do you do?"), "o", "2nd example");
assert.equal(mostWanted("One"), "e", "3rd example");
assert.equal(mostWanted("Oops!"), "o", "4th example");
assert.equal(mostWanted("AAaooo!!!!"), "a", "Letters");

function mostWanted(array) {
    array = array.toLowerCase().split('').sort().filter(a => { return a <= 'z' && a >= 'a'});
    let c = {};

    for(let el of array) {
        if(c[el] === undefined) {
            c[el] = 1;
        } else {
            ++c[el];
        }
    }

    let r = array[0];
    for(let el in c) {
        if(c[el] > c[r]) {
            r = el;
        }
    }

    return r;
}