/*
Help Stephen to create a module for converting a normal time string to a morse time string. As you can see in the illustration, a gray circle means on, while a white circle means off. Every digit in the time string contains a different number of slots. The first digit for the hours has a length of 2 while the second digit for the hour has a length of 4. The first digits for the minutes and seconds have a length of 3 while the second digits for the minutes and seconds have a length of 4. Every digit in the time is converted to binary representation. You will convert every on (or 1) signal to dash ("-") and every off (or 0) signal to dot (".").

source: Wikipedia

An time string could be in the follow formats: "hh:mm:ss", "h:m:s" or "hh:m:ss". The "missing" digits are zeroes. For example, "1:2:3" is the same as "01:02:03".

The result will be a morse time string with specific format:
"h h : m m : s s"
where each digits represented as sequence of "." and "-"

Input: A normal time string as a string (unicode).

Output: The morse time string as a string.

Precondition:
time_string contains correct time.
 */

let assert = require('assert');

function toBits(number, nBits) {
    let r = [];
    for(let i = 0; i < nBits; i++) {
        r.push((number >> i) & 1);
    }
    return r;
}

function morseClock(data) {
    let els = data.split(':');
    for(let i = 0; i < els.length; i++) {
        if(els[i].length !== 2) {
            els[i] = '0' + els[i];
        }
    }
    els = els.join('');
    let bits = [2, 4, 3, 4, 3, 4];
    let res = "";
    for(let i = 0; i < bits.length; i++) {
        let morseBits = toBits(parseInt(els.charAt(i)), bits[i]);
        for(let j = morseBits.length - 1; j >= 0; j--) {
            res += morseBits[j] ? '-' : '.'
        }
        res += ' ';

        if(i !== bits.length - 1 && i % 2) {
            res += ': '
        }
    }

    return res.trim();
}

assert.equal(morseClock("10:37:49"), ".- .... : .-- .--- : -.. -..-", "1st");
assert.equal(morseClock("21:34:56"), "-. ...- : .-- .-.. : -.- .--.", "2nd");
assert.equal(morseClock("00:1:02"), ".. .... : ... ...- : ... ..-.", "3rd");
assert.equal(morseClock("23:59:59"), "-. ..-- : -.- -..- : -.- -..-", "4th");
