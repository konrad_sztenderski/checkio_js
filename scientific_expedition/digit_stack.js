/*
In computer science, a stack is a particular kind of data type or collection in which the principal operations in the collection are the addition of an entity to the collection (also known as push) and the removal of an entity (also known as pop). The relation between the push and pop operations is such that the stack is a Last-In-First-Out (LIFO) data structure. In a LIFO data structure, the last element added to the structure must be the first one to be removed. Often a peek, or top operation is also implemented, returning the value of the top element without removing it.

We will emulate the stack process with Python. You are given a sequence of commands:
- "PUSH X" -- add X in the stack, where X is a digit.
- "POP" -- look and remove the top position. If the stack is empty, then it returns 0 (zero) and does nothing.
- "PEEK" -- look at the top position. If the stack is empty, then it returns 0 (zero).
The stack can only contain digits.

You should process all commands and sum all digits which were taken from the stack ("PEEK" or "POP"). Initial value of the sum is 0 (zero).

Input: A sequence of commands as a list of strings.

Output: The sum of the taken digits as an integer.

Precondition:
0 ≤ len(commands) ≤ 20;
all(re.match("\APUSH \d\Z", c) or с == "POP" or c == "PEEK" for c in commands)


 */

let assert = require('assert');

function digitStack(commands){
    let stack = [];
    let sum = 0;

    for(let command of commands) {
        let com = command.split(' ');

        switch(com[0]) {
            case 'PUSH': {
                stack.push(parseInt(com[1]));
                break;
            }
            case 'POP': {
                let el = stack.pop();
                sum += el === undefined ? 0 : el;
                break;
            }
            case 'PEEK': {
                let el = stack[stack.length - 1];
                sum += el === undefined ? 0 : el;
                break;
            }
        }
    }

    return sum;
}

console.log("Example:");
console.log(digitStack(["PUSH 3", "POP", "POP", "PUSH 4", "PEEK",
    "PUSH 9", "PUSH 0", "PEEK", "POP", "PUSH 1", "PEEK"]));

assert.equal(digitStack(["PUSH 3", "POP", "POP", "PUSH 4", "PEEK",
        "PUSH 9", "PUSH 0", "PEEK", "POP", "PUSH 1", "PEEK"]),
    8, "Example");
assert.equal(digitStack(["POP", "POP"]), 0, "pop, pop, zero");
assert.equal(digitStack(["PUSH 9", "PUSH 9", "POP"]), 9, "Push the button");
assert.equal(digitStack([]), 0, "Nothing");