/*
Nicola's discovered a calliper inside a set of drafting tools he received as a gift. Seeing the caliper, he has decided to learn how to use it.

Through any three points that don't exist on the same line, there lies a unique circle. The points of this circle are represented in a string with the coordinates like so:

    "(x1,y1),(x2,y2),(x3,y3)"

Where x1,y1,x2,y2,x3,y3 are digits.

You should find the circle for the three given points, such that the circle lies through these point and return the result as a string with the equation of the circle. In a Cartesian coordinate system (with an X and Y axis), the circle with central coordinates of (x0,y0) and radius of r can be described with the following equation:

    "(x-x0)^2+(y-y0)^2=r^2"

where x0,y0,r are decimal numbers rounded to two decimal points. Remove extraneous zeros and all decimal points, they are not necessary. For rounding, use the standard mathematical rules.

Input: Coordinates as a string.

Output: The equation of the circle as a string.

Precondition: All three given points do not lie on one line.
0 < xi, yi, r < 10
 */
let assert = require('assert');

function strToCoords(strCoords) {
    let reg = /\(([0-9]+),([0-9]+)\)/g;
    let coords= [];
    let el;
    while((el = reg.exec(strCoords))) {
        coords.push({x: parseInt(el[1]), y: parseInt(el[2])})
    }

    return coords;
}

function getBisection(a, b) {
    let center = {
        x: (a.x + b.x) / 2,
        y: (a.y + b.y) / 2
    };
    let lineA = -1 / ((a.y - b.y) / (a.x - b.x));
    return {
        center,
        line: {
            a: lineA,
            b: (center.y - lineA * center.x)
        }
    };
}

function equation(coords) {
    let max = 0;
    let points = {};

    for(let i = 0; i < coords.length; i++) {
        for(let j = i; j < coords.length; j++) {
            let d = Math.sqrt((coords[j].x - coords[i].x) ** 2 + (coords[j].y - coords[i].y) ** 2);
            if(d > max) {
                max = d;
                points.a = i;
                points.b = j;
            }
        }
    }

    for(let i = 0; i < coords.length; i++) {
        if(i !== points.a && i !== points.b) {
            points.c = i;
            break;
        }
    }

    /* bisection of a, b line */
    let bisection_1 = getBisection(coords[points.a], coords[points.b]);
    /* bisection of a, c line */
    let bisection_2 = getBisection(coords[points.a], coords[points.c]);
    /* bisection of b, c line */
    let bisection_3 = getBisection(coords[points.b], coords[points.c]);

    if(bisection_1.line.a === -Infinity || bisection_1.line.a === Infinity) {
        bisection_1 = bisection_2;
        bisection_2 = bisection_3;
    } else if(bisection_2.line.a === -Infinity || bisection_2.line.a === Infinity) {
        bisection_2 = bisection_3;
    }

    let x = (bisection_1.line.b - bisection_2.line.b) / (bisection_2.line.a - bisection_1.line.a);
    let y = bisection_1.line.a * x + bisection_1.line.b;

    let r = Math.sqrt((x - coords[points.c].x) ** 2 + (y - coords[points.c].y) ** 2);

    x = Math.round(x * 100) / 100;
    y = Math.round(y * 100) / 100;
    r = Math.round(r * 100) / 100;
    return `(x-${x})^2+(y-${y})^2=${r}^2`;
}

function circle(data) {
    return equation(strToCoords(data));
}

assert.equal(circle("(2,2),(6,2),(2,6)"), "(x-4)^2+(y-4)^2=2.83^2");
assert.equal(circle("(3,7),(6,9),(9,7)"), "(x-6)^2+(y-5.75)^2=3.25^2");