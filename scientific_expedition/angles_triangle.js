/*
You are given the lengths for each side on a triangle. You need to find all three angles for this triangle. If the given side lengths cannot form a triangle (or form a degenerated triangle), then you must return all angles as 0 (zero). The angles should be represented as a list of integers in ascending order. Each angle is measured in degrees and rounded to the nearest integer number (Standard mathematical rounding).

Input: The lengths of the sides of a triangle as integers.

Output: Angles of a triangle in degrees as sorted list of integers.

Precondition:
0 < a,b,c ≤ 1000
 */
let assert = require('assert');

function calcAngle(a, b, c) {
    let r = Math.acos((c ** 2 - a ** 2 - b ** 2) / (-2 * a * b)) * 180 / Math.PI;
    return Math.round(r);
}

function triangleAngles(a, b, c){
    if(a + b <= c || a + c <= b || b + c <= a)
        return [0, 0, 0];

    return [calcAngle(b, c, a), calcAngle(a, c, b), calcAngle(a, b, c)].sort((a, b) => a - b);
}

assert.deepEqual(triangleAngles(11, 20, 30), [11, 20, 149], "All sides are equal");
assert.deepEqual(triangleAngles(3, 4, 5), [37, 53, 90], "Egyptian triangle");
assert.deepEqual(triangleAngles(2, 2, 5), [0, 0, 0], "It's can not be a triangle");
