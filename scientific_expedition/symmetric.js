/*
In mathematics, particularly in linear algebra, a skew-symmetric matrix (also known as an antisymmetric or antimetric) is a square matrix A which is transposed and negative. This means that it satisfies the equation A = −AT. If the entry in the i-th row and j-th column is aij, i.e. A = (aij) then the symmetric condition becomes aij = −aji.

You should determine whether the specified square matrix is skew-symmetric or not.

You can find more details on Skew-symmetric matrices on its Wikipedia page.

Input: A square matrix as a list of lists with integers.

Output: If the matrix is skew-symmetric or not as a boolean.

Precondition: 0 < N < 5
 */
let assert = require('assert');

function symmetric(matrix) {
    for(let i = 0; i < matrix.length; i++) {
        for(let j = 0; j < matrix.length; j++) {
            if(matrix[i][j] !== -matrix[j][i]) {
                return false;
            }
        }
    }

    return true;
}

console.log('Example:');
console.log(symmetric([
    [0, 1, 2],
    [-1, 0, 1],
    [-2, -1, 0]]));

// These "asserts" using only for self-checking and not necessary for auto-testing

assert.equal(symmetric([
    [0, 1, 2],
    [-1, 0, 1],
    [-2, -1, 0]]), true);
assert.equal(symmetric([
    [0, 1, 2],
    [-1, 1, 1],
    [-2, -1, 0]]), false);
assert.equal(symmetric([
    [0, 1, 2],
    [-1, 0, 1],
    [-3, -1, 0]]), false);
